package recursividade;

public class Questao03 {

	public static void main(String[] args) {
		System.out.println(BinarioDecimal(8));
	}

	private static String BinarioDecimal(int valor) {

		String resultado = "";
		if (valor > 0) {
			resultado = "" + valor % 2;
			valor = valor / 2;

			resultado = BinarioDecimal(valor) + resultado;
		}

		return resultado;
	}
}
