package recursividade;

import java.util.Scanner;

public class Questao02 {

	static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		System.out.println("Informe um numero inteiro");
		int numero = sc.nextInt();
		System.out.println(fatorial(numero));

	}

	public static int fatorial(int numero) {
		int resulFatorial = 1;
		if (numero == 0) {
			return 1;
		} else if (numero == 1) {
			return 1;
		} else {
			resulFatorial = numero * fatorial(numero-1);
		}
			return resulFatorial;
		
	}

}
