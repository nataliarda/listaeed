package recursividade;

public class Questao04 {

	public static void main(String[] args) {
		System.out.println("Resultado: " + somar("100"));
	}

	private static int somar(String string) {

		int resultado = 0;

		String valor = string.substring(0, 1);
		resultado += Integer.parseInt(valor);
		string = string.substring(string.length() - (string.length() - 1));
		if (string.length() != 0) {
			resultado += somar(string);
		}

		return resultado;
	}
}
