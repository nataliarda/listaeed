package recursividade;

public class Questao01 {

	public static void main (String [] args) {
		int resultado;
		resultado = CalculaMDC2(30,3);
		System.out.println(resultado);
	}
	public static int CalculaMDC2(int m, int n) {
		int r;
		r = m % n;
		m = n;
		n = r;
		if (r != 0) {
			m = CalculaMDC2(m, n);
		}
		return m;
	}
}
